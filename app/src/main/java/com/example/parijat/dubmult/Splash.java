package com.example.parijat.dubmult;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by Parijat on 16-08-2014.
 */
public class Splash extends Activity {
    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash);
        Thread timer = new Thread(){
          public void run(){

              try{
                    sleep(3000);
              }catch (InterruptedException e){
                    e.printStackTrace();
              }finally {

                  Intent openfirst = new Intent("android.intent.action.MYACTIVITY");
                  startActivity(openfirst);
              }
          }
        };
        timer.start();
    }
}
