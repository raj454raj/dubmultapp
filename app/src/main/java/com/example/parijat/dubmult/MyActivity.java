package com.example.parijat.dubmult;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MyActivity extends Activity {
    Button getStart;
    TextView display;
    Intent i;
    final static int cameraData=0;
    Uri videoUri;
    public final static String EXTRA_MESSAGE = "com.example.parijat.dubmult.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        getStart = (Button) findViewById(R.id.getstartbutton);
        display = (TextView) findViewById(R.id.textView1);
        getStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                startActivityForResult(i, cameraData);
               }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
        // TODO Auto-generated method stub
        super.onActivityResult(arg0, arg1, arg2);
        if(arg1 == RESULT_OK) {
            videoUri = arg2.getData();
            i = new Intent("android.intent.action.VIEWER");
            i.putExtra(EXTRA_MESSAGE,videoUri);
            startActivity(i);
        }
    }
}
