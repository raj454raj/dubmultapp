package com.example.parijat.dubmult;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.VideoView;
//import android.R;
/**
 * Created by Parijat on 17-08-2014.
 */
public class Viewer extends Activity {
    VideoView Video;
    Uri VideoUri;
    Button bvid;
    Button baud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show);
        bvid = (Button) findViewById(R.id.vidbutton);
        baud = (Button) findViewById(R.id.audbutton);

        Video = (VideoView) findViewById(R.id.videoView);
        Intent intent = getIntent();
        VideoUri = intent.getParcelableExtra(MyActivity.EXTRA_MESSAGE);
        Video.setVideoURI(VideoUri);

        bvid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringer;
                stringer = (String) bvid.getText();
                if (stringer == "Start Video") {
                    Video.start();

                    bvid.setText("Pause Video");
                } else {
                    Video.pause();
                    bvid.setText("Start Video");
                }
            }
        });
        baud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("android.intent.action.AUDIORECORDTEST");
                startActivity(i);

            }
        });

    }

}