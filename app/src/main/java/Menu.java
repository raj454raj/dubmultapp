import android.app.Activity;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by Parijat on 16-08-2014.
 */


public class Menu extends ListActivity {
    String classes[] = {"Splash", "MyActivity", "example1", "example2", "example3"};


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        setListAdapter(new ArrayAdapter<String>(Menu.this, 0, classes));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
